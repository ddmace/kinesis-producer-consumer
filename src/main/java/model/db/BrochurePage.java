package model.db;

import java.io.Serializable;

public class BrochurePage implements Serializable {
 public boolean getActive() {
  return active;
 }

 public boolean isActive() {
  return active;
 }

 public void setActive(boolean active) {
  this.active = active;
 }

 public int getPageNum() {
  return pageNum;
 }

 public void setPageNum(int pageNum) {
  this.pageNum = pageNum;
 }

 public String getText() {
  return text;
 }

 public void setText(String text) {
  this.text = text;
 }

 private boolean active;
 private int pageNum;
 private String text;
}
