package model.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Brochure implements Serializable {
 public boolean getActive() {
  return active;
 }

 public boolean isActive() {
  return active;
 }

 public void setActive(boolean active) {
  this.active = active;
 }

 public String getBrochureGroupIdentifier() {
  return brochureGroupIdentifier;
 }

 public void setBrochureGroupIdentifier(String brochureGroupIdentifier) {
  this.brochureGroupIdentifier = brochureGroupIdentifier;
 }

 public int getPopularity() {
  return popularity;
 }

 public void setPopularity(int popularity) {
  this.popularity = popularity;
 }

 public int getSearchBoost() {
  return searchBoost;
 }

 public void setSearchBoost(int searchBoost) {
  this.searchBoost = searchBoost;
 }

 public Boolean getIsBillable() {
  return isBillable;
 }

 public void setIsBillable(Boolean isBillable) {
  this.isBillable = isBillable;
 }

 public Date getPublished() {
  return published;
 }

 public void setPublished(Date published) {
  this.published = published;
 }

 public Date getCreated() {
  return created;
 }

 public void setCreated(Date created) {
  this.created = created;
 }

 public Date getValidFrom() {
  return validFrom;
 }

 public void setValidFrom(Date validFrom) {
  this.validFrom = validFrom;
 }

 public Date getValidTo() {
  return validTo;
 }

 public void setValidTo(Date validTo) {
  this.validTo = validTo;
 }

 public Date getPublishDate() {
  return publishDate;
 }

 public void setPublishDate(Date publishDate) {
  this.publishDate = publishDate;
 }

 public String getName() {
  return name;
 }

 public void setName(String name) {
  this.name = name;
 }

 public String getShortDescription() {
  return shortDescription;
 }

 public void setShortDescription(String shortDescription) {
  this.shortDescription = shortDescription;
 }

 public int getNumPages() {
  return numPages;
 }

 public void setNumPages(int numPages) {
  this.numPages = numPages;
 }

 public Boolean getHideFromExpiredSearchResults() {
  return hideFromExpiredSearchResults;
 }

 public void setHideFromExpiredSearchResults(Boolean hideFromExpiredSearchResults) {
  this.hideFromExpiredSearchResults = hideFromExpiredSearchResults;
 }

 public Boolean getWebActive() {
  return webActive;
 }

 public void setWebActive(Boolean webActive) {
  this.webActive = webActive;
 }

 public Boolean getMobileWebActive() {
  return mobileWebActive;
 }

 public void setMobileWebActive(Boolean mobileWebActive) {
  this.mobileWebActive = mobileWebActive;
 }

 public Boolean getAndroidActive() {
  return androidActive;
 }

 public void setAndroidActive(Boolean androidActive) {
  this.androidActive = androidActive;
 }

 public Boolean getIpadActive() {
  return ipadActive;
 }

 public void setIpadActive(Boolean ipadActive) {
  this.ipadActive = ipadActive;
 }

 public Boolean getIphoneActive() {
  return iphoneActive;
 }

 public void setIphoneActive(Boolean iphoneActive) {
  this.iphoneActive = iphoneActive;
 }

 public Boolean getCdnUploadErrors() {
  return cdnUploadErrors;
 }

 public void setCdnUploadErrors(Boolean cdnUploadErrors) {
  this.cdnUploadErrors = cdnUploadErrors;
 }

 public String getCdnUploadErrorString() {
  return cdnUploadErrorString;
 }

 public void setCdnUploadErrorString(String cdnUploadErrorString) {
  this.cdnUploadErrorString = cdnUploadErrorString;
 }

 public String getFolderName() {
  return folderName;
 }

 public void setFolderName(String folderName) {
  this.folderName = folderName;
 }

 public String getPdfFileName() {
  return pdfFileName;
 }

 public void setPdfFileName(String pdfFileName) {
  this.pdfFileName = pdfFileName;
 }

 public boolean getImagesUploadedInCDN() {
  return imagesUploadedInCDN;
 }

 public boolean isImagesUploadedInCDN() {
  return imagesUploadedInCDN;
 }

 public void setImagesUploadedInCDN(boolean imagesUploadedInCDN) {
  this.imagesUploadedInCDN = imagesUploadedInCDN;
 }

 public String getFrontPageImageFileName() {
  return frontPageImageFileName;
 }

 public void setFrontPageImageFileName(String frontPageImageFileName) {
  this.frontPageImageFileName = frontPageImageFileName;
 }

 public int getFrontPageImageWidth() {
  return frontPageImageWidth;
 }

 public void setFrontPageImageWidth(int frontPageImageWidth) {
  this.frontPageImageWidth = frontPageImageWidth;
 }

 public int getFrontPageImageHeight() {
  return frontPageImageHeight;
 }

 public void setFrontPageImageHeight(int frontPageImageHeight) {
  this.frontPageImageHeight = frontPageImageHeight;
 }

 public List getStores() {
  return stores;
 }

 public void setStores(List stores) {
  this.stores = stores;
 }

 public String getHorizontalImageFileName() {
  return horizontalImageFileName;
 }

 public void setHorizontalImageFileName(String horizontalImageFileName) {
  this.horizontalImageFileName = horizontalImageFileName;
 }

 public Integer getHorizontalImageWidth() {
  return horizontalImageWidth;
 }

 public void setHorizontalImageWidth(Integer horizontalImageWidth) {
  this.horizontalImageWidth = horizontalImageWidth;
 }

 public Integer getHorizontalImageHeight() {
  return horizontalImageHeight;
 }

 public void setHorizontalImageHeight(Integer horizontalImageHeight) {
  this.horizontalImageHeight = horizontalImageHeight;
 }

 public String getMainVideoFileName() {
  return mainVideoFileName;
 }

 public void setMainVideoFileName(String mainVideoFileName) {
  this.mainVideoFileName = mainVideoFileName;
 }

 public String getBannerImage() {
  return bannerImage;
 }

 public void setBannerImage(String bannerImage) {
  this.bannerImage = bannerImage;
 }

 private boolean active;
 private String brochureGroupIdentifier;
 private int popularity;
 private int searchBoost;
 private Boolean isBillable;
 private Date published;
 private Date created;
 private Date validFrom;
 private Date validTo;
 private Date publishDate;
 private String name;
 private String shortDescription;
 private int numPages;
 private Boolean hideFromExpiredSearchResults;
 private Boolean webActive;
 private Boolean mobileWebActive;
 private Boolean androidActive;
 private Boolean ipadActive;
 private Boolean iphoneActive;
 private Boolean cdnUploadErrors;
 private String cdnUploadErrorString;
 private String folderName;
 private String pdfFileName;
 private boolean imagesUploadedInCDN;
 private String frontPageImageFileName;
 private int frontPageImageWidth;
 private int frontPageImageHeight;
 private List stores = new ArrayList();
 private String horizontalImageFileName;
 private Integer horizontalImageWidth;
 private Integer horizontalImageHeight;
 private String mainVideoFileName;
 private String bannerImage;
}
