package kinesis.consumer;

import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessorFactory;

public class SampleRecordProcessorFactory implements IRecordProcessorFactory {
    public SampleRecordProcessorFactory() {
        super();
    }

    public IRecordProcessor createProcessor() {
        return new RecordProcessor();
    }

}
