package avro;

import org.apache.avro.Schema;
import org.apache.avro.io.*;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.avro.specific.SpecificRecord;
import org.apache.avro.util.ByteBufferInputStream;
import org.apache.avro.util.ByteBufferOutputStream;
import org.apache.commons.io.output.ByteArrayOutputStream;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * This Singleton is responsible for serialize / deserializeJSON Avro Objects
 */
public class AvroSerializer {
    private AvroSerializer() {
    }

    public static synchronized AvroSerializer instance() {
        if (instance == null) instance = new AvroSerializer();

        return instance;
    }

    /**
     * Deserializes the given array of bytes using a JsonDecoder according to the given Schema
     *
     * @param avroBytes Array of bytes trepresenting a json String
     * @param clazz Class to be created
     * @param schema Schema to be used
     * @return An instance of the given clazz
     */
    public <T> T deserializeJSON( byte[] avroBytes, Class<T> clazz, Schema schema ) throws IOException {
        DataInputStream dis = null;

        try {
            dis = new DataInputStream(new ByteArrayInputStream(avroBytes));
            JsonDecoder decoder = DecoderFactory.get().jsonDecoder(schema, dis);
            SpecificDatumReader<T> reader = new SpecificDatumReader<T>(clazz);
            return reader.read(null, decoder);
        } finally {
            try {dis.close();} catch (IOException ignore) {}
        }
    }

    public <T> T deserializeJSON( String json, Class<T> clazz, Schema schema ) throws IOException {
        JsonDecoder decoder = DecoderFactory.get().jsonDecoder(schema, json);
        SpecificDatumReader<T> reader = new SpecificDatumReader<>(clazz);
        return reader.read(null, decoder);
    }

    // this version does not need the Schema
    public <T> T deserializeJSON( String json, Class<T> clazz ) throws IOException {
        SpecificDatumReader<T> reader = new SpecificDatumReader<>(clazz);
        Schema s = reader.getSpecificData().getSchema(clazz);
        JsonDecoder decoder = DecoderFactory.get().jsonDecoder(s, json);
        return reader.read(null, decoder);
    }

    /**
     * Deserializes the given array of bytes using a Binary decoder according to the Class
     *
     * @param avroBytes
     * @param clazz
     * @param <T>
     * @return
     * @throws IOException
     */
    public <T> T deserialize( byte[] avroBytes, Class<T> clazz ) throws IOException {
        Decoder decoder = DecoderFactory.get().binaryDecoder( avroBytes, null );
        SpecificDatumReader<T> reader = new SpecificDatumReader<>(clazz);
        return reader.read(null, decoder);
    }


    /**
     * Deserializes the given array of bytes using a DirectBinaryDecoder according to the given Schema
     *
     * @param buffer ByteBuffer with payload to be deserialized
     * @param schema Schema to be used
     * @return <T>
     * @throws IOException
     */
    public <T> T deserialize( ByteBuffer buffer, Schema schema ) throws IOException {
        Decoder decoder = DecoderFactory.get().directBinaryDecoder( toInputStream(buffer), null );
        SpecificDatumReader<T> reader = new SpecificDatumReader<>( schema );
        return reader.read(null, decoder);
    }

    /**
     *
     *
     * @param clazz
     * @param object
     * @param <T>
     * @return <T>
     */
    public <T> ByteBuffer serialize( Class<T> clazz, Object object ) throws IOException {
        if (object == null || !(object instanceof SpecificRecord)) {
            return null;
        }
        T record = (T) object;

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        Encoder encoder = EncoderFactory.get().directBinaryEncoder( out, null );
        SpecificDatumWriter<T> w = new SpecificDatumWriter<>(clazz);
        w.write(record, encoder);
        encoder.flush();
        ByteBuffer serialized = ByteBuffer.wrap( out.toByteArray() );

        return serialized;
    }

    /**
     *
     *
     * @param clazz
     * @param object
     * @param <T>
     * @return <T>
     */
    public <T> String serializeJSON( Class<T> clazz, Object object, Schema schema ) throws IOException {
        if (object == null || !(object instanceof SpecificRecord)) {
            return null;
        }
        T record = (T) object;

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        JsonEncoder encoder = EncoderFactory.get().jsonEncoder( schema, out );
        SpecificDatumWriter<T> w = new SpecificDatumWriter<>(clazz);
        w.write(record, encoder);
        encoder.flush();

        return out.toString( "UTF-8" );
    }

    private ByteBufferInputStream toInputStream( ByteBuffer buffer ) {
        List<ByteBuffer> bytes = new ArrayList<>(1);
        bytes.add( buffer );
        return new ByteBufferInputStream(bytes);
    }

    private static AvroSerializer instance;
}