package model.mapper;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.converter.ConverterFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import model.avro.AvroBrochure;
import model.avro.AvroStore;
import model.db.Brochure;
import model.db.Store;

/**
 * Created by marina on 8/08/16.
 */
public final class MapperConfig {
    private static final MapperFactory mapperFactory;

    static {
        mapperFactory = new DefaultMapperFactory.Builder().build();

        // register custom converters
        ConverterFactory converterFactory = mapperFactory.getConverterFactory();
        converterFactory.registerConverter("dateToLongConverter", new DateToLongConverter());

        // Map Store
        mapperFactory.classMap(Store.class, AvroStore.class).byDefault().register();

        // Map Brochure
        mapperFactory.classMap(Brochure.class, AvroBrochure.class)
                //.mapNulls(true)
                .mapNullsInReverse(true)
                .field("name", "capitalizedName")
                .fieldMap("published", "published").converter("dateToLongConverter").add()
                .fieldMap("created").converter("dateToLongConverter").add()
                .fieldMap("validFrom").converter("dateToLongConverter").add()
                .fieldMap("validTo").converter("dateToLongConverter").add()
                .fieldMap("publishDate").converter("dateToLongConverter").add()
                /*.customize(new CustomMapper<Brochure, AvroBrochure>() {
                    @Override
                    public void mapAtoB(Brochure source, AvroBrochure target, MappingContext context) {
                    }

                    @Override
                    public void mapBtoA( AvroBrochure source, Brochure target, MappingContext c) {
                    }
                })*/
                .byDefault().register();
    }

    private MapperConfig() {}

    public static MapperFacade getMapperFacade() {
        return mapperFactory.getMapperFacade();
    }

}
