package kinesis.consumer.utils;

import avro.AvroSerializer;
import com.amazonaws.services.kinesis.AmazonKinesisClient;
import com.amazonaws.services.kinesis.model.*;
import model.avro.AvroBrochure;
import model.avro.AvroEvent;
import model.avro.AvroMsg;
import model.dto.AvroEntityTypes;
import org.apache.avro.Schema;
import org.apache.avro.reflect.ReflectData;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.List;

/**
 *
 */
public class Describe {

    public static void main(String[] args) {

        serializer = AvroSerializer.instance();

        AmazonKinesisClient client = new AmazonKinesisClient();
        try {

            //************************************************
            // 1. list streams
            //************************************************

            log.info( "Conecting to " + ENDPOINT + " ...");

            client.setEndpoint(ENDPOINT);
            // client.setRegion( new Region() )

            ListStreamsRequest listStreamsRequest = new ListStreamsRequest();
            listStreamsRequest.setLimit(20);
            ListStreamsResult listStreamsResult = client.listStreams(listStreamsRequest);
            List<String> streamNames = listStreamsResult.getStreamNames();

            log.info( "-- Available streams: ");
            for( String name: streamNames ) {
                log.info( name );
            }
            log.info( "----------------------");

            //************************************************
            // 2. get shard iterator
            //************************************************
            //String shardIterator = getShardIterator(client, ITERATOR_TYPE);

            String shardIterator = getShardIteratorFromSN( client, "49564633418381140073548982202826746514764243375942008834" );

            //************************************************
            // 3. read data from given shard iterator
            //************************************************
            log.info( "waiting for data");
            int requestNum = 0;

            while ( requestNum < 500 ) {
                GetRecordsResult result = getRecords(client, shardIterator);

                if ( result == null || result.getRecords() == null || result.getRecords().size() == 0 ) {
                    //println "no records..."
                    System.out.print( ".");
                    if ( (requestNum % 100) == 0) {
                        System.out.println( "" );
                    }

                } else {
                    System.out.println( "" );
                    System.out.println( String.valueOf( requestNum + 1) );

                    for( Record record: result.getRecords() ) {
                        processSingleRecord( record );
                    }
                }

                shardIterator = result.getNextShardIterator();
                try {
                    Thread.sleep( 1000 );
                } catch (InterruptedException ignore) {
                }

                requestNum++;
            }


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            client.shutdown();
        }


    }

    private static GetRecordsResult getRecords(AmazonKinesisClient client, String shardIterator) {
        GetRecordsRequest getRecordsRequest = new GetRecordsRequest();
        getRecordsRequest.setShardIterator(shardIterator);
        getRecordsRequest.setLimit(25);

        return client.getRecords(getRecordsRequest);
    }

    private static String getShardIterator(AmazonKinesisClient client, String iteratorType) {
        log.info( "getting shard iterator from " + serviceName + " and " + SHARD_ID + " and " + iteratorType);

        GetShardIteratorRequest getShardIteratorRequest = new GetShardIteratorRequest();
        getShardIteratorRequest.setStreamName(serviceName);
        getShardIteratorRequest.setShardId(SHARD_ID);
        getShardIteratorRequest.setShardIteratorType(iteratorType);

        GetShardIteratorResult getShardIteratorResult = client.getShardIterator(getShardIteratorRequest);
        return getShardIteratorResult.getShardIterator();
    }

    /**
     * Retrieves a shard iterator from a specific sequence number
     * @param client AmazonKinesisClient to use
     * @param seqNumber SequenceNumber to seek
     * @return String with the Shard iterator value
     */
    private static String getShardIteratorFromSN(AmazonKinesisClient client, String seqNumber) {
        log.info( "getting shard iterator from " + serviceName + " and " + SHARD_ID + " and " + seqNumber);

        GetShardIteratorRequest getShardIteratorRequest = new GetShardIteratorRequest();
        getShardIteratorRequest.setShardIteratorType( ShardIteratorType.AFTER_SEQUENCE_NUMBER );
        getShardIteratorRequest.setStartingSequenceNumber( seqNumber );
        getShardIteratorRequest.setStreamName( serviceName );
        getShardIteratorRequest.setShardId( SHARD_ID );

        GetShardIteratorResult getShardIteratorResult = client.getShardIterator( getShardIteratorRequest );
        return getShardIteratorResult.getShardIterator();
    }

    private static void processSingleRecord(final Record record) throws IOException {
        try {

            Schema schema = ReflectData.get().getSchema( AvroMsg.class );
            final AvroMsg msg = serializer.deserialize(record.getData(), schema);
            log.info( "received AvroMsg: with schema-id: " + msg.getSchemaId() + ", action: " + msg.getAction());

            if( msg.getSchemaId().equals( AvroEntityTypes.BROCHURE.getId()) ) {

                schema = ReflectData.get().getSchema(AvroBrochure.class);
                final AvroBrochure event = serializer.deserializeJSON(msg.getPayload().toString(), AvroBrochure.class, schema);

                log.info( "received AvroBrochure: \'" + String.valueOf(event.getName()) + "\'");

            } else if( msg.getSchemaId().equals( AvroEntityTypes.EVENT.getId() )) {
                schema = ReflectData.get().getSchema(AvroEvent.class);
                final AvroEvent event = serializer.deserializeJSON(msg.getPayload().toString(), AvroEvent.class, schema);

                log.info( "received AvroEvent: \'" + String.valueOf(event.getMessage()) + "\'");
            }


            log.info(record.getSequenceNumber() + ", " + record.getPartitionKey() );

        } catch (CharacterCodingException e) {
            log.error("Malformed data: ", e);
        }

        log.info( record.getPartitionKey() + " - " + record.getSequenceNumber());



    }

    private static final Log log = LogFactory.getLog( Describe.class );
    private static final String ENDPOINT = "kinesis.eu-west-1.amazonaws.com";
    private static final String serviceName = "TestStream";
    private static final String ITERATOR_TYPE = "TRIM_HORIZON";
    private static final String SHARD_ID = "shardId-000000000000";

    private static final String regionId = "eu-west-1";
    private static final CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();
    private static AvroSerializer serializer;
    private static Schema schema;
}
