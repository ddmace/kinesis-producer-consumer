import model.avro.AvroBrochure;
import model.avro.AvroEvent;
import model.avro.AvroStore;
import model.dto.AvroEntityTypes;
import kinesis.producer.EventsProducerToKinesis;
import model.dto.ProducerDto;

import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by marina on 2/08/16.
 */
public class AvroEventsProducerInteg {
    public static void main (String[] args) throws InterruptedException {

        BlockingQueue<ProducerDto> inputQueue = new ArrayBlockingQueue<ProducerDto>(3);
        AvroEvent ae = new AvroEvent(new Date().getTime(), "huola1");
        inputQueue.add(new ProducerDto("AvroEvent", "new", AvroEntityTypes.EVENT, ae));

        AvroBrochure brochure = new AvroBrochure();
        brochure.setActive(true);
        brochure.setBrochureGroupIdentifier("BrochureGroupIdentifier");
        brochure.setPopularity(3);
        brochure.setSearchBoost(1);
        brochure.setIsBillable(true);
        brochure.setPublished(123456789L);
        brochure.setCreated(987654321L);
        brochure.setValidFrom(741258963L);
        brochure.setValidTo(987123654L);
        brochure.setPublishDate(1597536482L);
        brochure.setName("Name2");
        brochure.setShortDescription("ShortDescription");
        brochure.setNumPages(9);
        brochure.setHideFromExpiredSearchResults(true);
        brochure.setWebActive(false);
        brochure.setMobileWebActive(true);
        brochure.setAndroidActive(true);
        brochure.setIpadActive(true);
        brochure.setIphoneActive(false);
        brochure.setCdnUploadErrors(false);
        brochure.setCdnUploadErrorString("CdnUploadErrorString");
        brochure.setFolderName("FolderName");
        brochure.setPdfFileName("PdfFileName");
        brochure.setImagesUploadedInCDN(true);
        brochure.setFrontPageImageFileName("FrontPageImageFileName");
        brochure.setFrontPageImageWidth(300);
        brochure.setFrontPageImageHeight(400);
        brochure.setStores(Arrays.asList(new AvroStore("name", "email", "web", "telephone", "fax", "description")));
        brochure.setHorizontalImageFileName("HorizontalImageFileName");
        brochure.setHorizontalImageWidth(514);
        brochure.setHorizontalImageHeight(600);
        brochure.setMainVideoFileName("MainVideoFileName");
        brochure.setBannerImage("BannerImage");
        brochure.setCapitalizedName("CapitalizedName");
        brochure.setNameWithRetailer("NameWithRetailer");

        inputQueue.add(new ProducerDto("AvroBrochure","update", AvroEntityTypes.BROCHURE, brochure));
        //inputQueue.add(new ProducerDto("AvroBrochure","new", 1L, new AvroBrochure()));
        EventsProducerToKinesis producer = new EventsProducerToKinesis(inputQueue);
        producer.run();
        Thread.sleep(5000);
        producer.stop();
    }
}
