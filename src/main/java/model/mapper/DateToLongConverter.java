package model.mapper;

import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

import java.util.Date;

/**
 * Created by marina on 9/08/16.
 */
public class DateToLongConverter extends BidirectionalConverter<Date, Long> {

    public Long convertTo(Date source, Type<Long> destinationType) {
        if (source != null) {
            return source.getTime();
        }
        return 0L;
    }

    public Date convertFrom(Long source, Type<Date> destinationType) {
        if (source != null) {
            return new Date(source);
        }
        return null;
    }
}
