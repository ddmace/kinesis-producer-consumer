package kinesis.producer;

import model.dto.ProducerDto;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by marina on 2/08/16.
 */
public abstract class AbstractEventsProducer implements Runnable {

    protected final static String STREAM_NAME = "TestStream";
    protected final static String REGION = "eu-west-1";

    protected final BlockingQueue<ProducerDto> inputQueue;
    protected volatile boolean shutdown = false;
    protected final AtomicLong recordsPut = new AtomicLong(0);

    protected AbstractEventsProducer(BlockingQueue<ProducerDto> inputQueue) {
        this.inputQueue = inputQueue;
    }

    public void run() {
        while (!shutdown) {
            try {
                runOnce();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public long recordsPut() {
        return recordsPut.get();
    }

    public void stop() {
        shutdown = true;
    }

    protected abstract void runOnce() throws Exception;
}