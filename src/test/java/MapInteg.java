import ma.glasnost.orika.MapperFacade;
import model.avro.AvroBrochure;
import model.db.Brochure;
import model.db.Store;
import model.mapper.MapperConfig;

import java.util.Arrays;
import java.util.Date;

/**
 * Test for Map objects with orika
 *
 * Created by marina on 8/08/16.
 */
public class MapInteg {
    public static void main (String[] args) {
        Brochure brochure = new Brochure();
        brochure.setActive(true);
        brochure.setBrochureGroupIdentifier("BrochureGroupIdentifier");
        brochure.setPopularity(3);
        brochure.setSearchBoost(1);
        brochure.setIsBillable(true);
        brochure.setPublished(new Date());
        brochure.setCreated(new Date());
        brochure.setValidFrom(new Date());
        brochure.setValidTo(new Date());
        brochure.setPublishDate(new Date());
        brochure.setName("Name");
        brochure.setShortDescription("ShortDescription");
        brochure.setNumPages(9);
        brochure.setHideFromExpiredSearchResults(true);
        brochure.setWebActive(false);
        brochure.setMobileWebActive(true);
        brochure.setAndroidActive(true);
        brochure.setIpadActive(true);
        brochure.setIphoneActive(false);
        brochure.setCdnUploadErrors(false);
        brochure.setCdnUploadErrorString("CdnUploadErrorString");
        brochure.setFolderName("FolderName");
        brochure.setPdfFileName("PdfFileName");
        brochure.setImagesUploadedInCDN(true);
        brochure.setFrontPageImageFileName("FrontPageImageFileName");
        brochure.setFrontPageImageWidth(300);
        brochure.setFrontPageImageHeight(400);
        Store store = new Store();
        store.setDescription("StoreDescription");
        store.setEmail("StoreEmail");
        store.setFax("StoreFax");
        store.setName("StoreName");
        store.setTelephone("StoreTelephone");
        store.setWeb("StoreWeb");
        brochure.setStores(Arrays.asList(store));
        brochure.setHorizontalImageFileName("HorizontalImageFileName");
        brochure.setHorizontalImageWidth(514);
        brochure.setHorizontalImageHeight(600);
        brochure.setMainVideoFileName("MainVideoFileName");
        brochure.setBannerImage("BannerImage");

        MapperFacade mapper = MapperConfig.getMapperFacade();

        AvroBrochure avroBrochure = mapper.map(brochure, AvroBrochure.class);
        System.out.println("avroBrochure: " + avroBrochure.getCapitalizedName());

        Brochure brochureRemapped = mapper.map(avroBrochure, Brochure.class);
        System.out.println("brochureRemapped: " + brochureRemapped.getName());
    }
}
