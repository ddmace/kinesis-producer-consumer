# Kinesis producer-consumer example

Example that contains the following features:
- Send / Writes an object to [Amazon Kinesis Streams](https://aws.amazon.com/es/kinesis/streams/)
- Receive / Read the object [Amazon Kinesis Streams](https://aws.amazon.com/es/kinesis/streams/)
- Given an object, mapped attributes in another using [Orika](http://orika-mapper.github.io/orika-docs/index.html) mapping configuration.
---
##### Version
1.0-SNAPSHOT

---
### Tech

Uses several sources:

* [Amazon kinesis producer library (KPL)](http://docs.aws.amazon.com/streams/latest/dev/developing-producers-with-kpl.html) -The Amazon Kinesis Producer Library (KPL) simplifies producer application development, allowing developers to achieve high write throughput to a Amazon Kinesis stream.
* [Amazon kinesis consumer library (KCL)](http://docs.aws.amazon.com/streams/latest/dev/developing-consumers-with-kcl.html) - Use the Streams API to get data from an Amazon Kinesis stream, we recommend using the design patterns and code for consumer applications provided by the KCL.
* [Orika](http://orika-mapper.github.io/orika-docs/intro.html) - This library provide solution in automating mapping using code generation. Code is generated in the manner like it would be written by developers to map one bean to another.


### Map an object
Define custom mapping rules for several objects in `MapperConfig` class.
``` java
  static {
        mapperFactory = new DefaultMapperFactory.Builder().build();

        // register custom converters
        ConverterFactory converterFactory = mapperFactory.getConverterFactory();
        converterFactory.registerConverter("dateToLongConverter", new DateToLongConverter());

        // Map Store
        mapperFactory.classMap(Store.class, AvroStore.class).byDefault().register();

        // Map Brochure
        mapperFactory.classMap(Brochure.class, AvroBrochure.class)
                .mapNullsInReverse(true)
                .field("name", "capitalizedName")
                .fieldMap("published", "published").converter("dateToLongConverter").add()
                .fieldMap("created").converter("dateToLongConverter").add()
                .fieldMap("validFrom").converter("dateToLongConverter").add()
                .fieldMap("validTo").converter("dateToLongConverter").add()
                .fieldMap("publishDate").converter("dateToLongConverter").add()
                .byDefault().register();
    }
```
Readmes:

* [Orika user guide](http://orika-mapper.github.io/orika-docs/intro.html)

### Kinesis
Example was develop taking in account the next  [limitations](http://docs.aws.amazon.com/streams/latest/dev/service-sizes-and-limits.html) in streams:

* The default shard limit is 50 shards for the following regions only:
  * US East (N. Virginia)
  * US West (Oregon)
  * EU (Ireland)
  * All other regions have a default shard limit of 25.
* Data records are accessible for a default of 24 hours from the time they are added to a stream. Configurable in hourly increments from 24 to 168 hours (1 to 7 days).
* The maximum size of a data blob (the data payload before Base64-encoding) is up to 1 MB.
* Each shard can support up to 1,000 records per second for writes, up to a maximum total data write rate of 1 MB per second (including partition keys). This write limit applies to operations such as PutRecord and PutRecords.
* Each shard can support up to 5 transactions per second for reads, up to a maximum total data read rate of 2 MB per second.
*  Next operations can provide up to 5 transactions per second:
  * GetShardIterator, CreateStream, DeleteStream, ListStreams, MergeShards and SplitShard
*  Next operations can provide up to 10 transactions per second:
  * DescribeStream
* GetRecords can retrieve 10 MB of data.
* A shard iterator returned by GetShardIterator times out after 5 minutes if you haven't used it.

#### Send object to kinesis
In this example, send to kinesis a list of objects in a single request/record, taking into account the limitations of the streams in kinesis.
##### Run it
Execute main class in `AvroEventsProducerInteg`.

##### How it works
Uses a DTO to wrap each object for allow add metadata about object and make it transparent to "publish/subscription" system. The DTO `AvroMsg` was automatic generated with [Avro](https://avro.apache.org/docs/1.8.1/gettingstartedjava.html#Serializing+and+deserializing+with+code+generation) from `message.avsc` file.
``` json
{
  "namespace": "model.avro",
  "type": "record",
  "name": "AvroMsg",
  "fields": [
    {"name": "timestamp", "type": "long", "default": "1"},
    {"name": "action", "type": "string", "default": "-"},
    {"name": "schemaId", "type": "long", "default": "1"},
    {"name": "payload", "type": "string", "default": "-"}
  ]
}
```

Build a list with the objects parsed to Avro and send to Kinesis producer process (integration test in `AvroEventsProducerInteg`).

``` java
 BlockingQueue<ProducerDto> inputQueue = new ArrayBlockingQueue<ProducerDto>(0);
 // add items parsed in Avro into list
 EventsProducerToKinesis producer = new EventsProducerToKinesis(inputQueue);
 producer.run();
```

The `EventsProducerToKinesis` process do:

Each object from the list is serialized in json and included into an `AvroMsg`. This `AvroMsg` is serializated again into array of bytes.
Then put in a kinesis record each `AvroMsg` serialized.

If message was successful sent, then the item kinesis details shown.

Readmes:

* All steps of [AWS KPL documentation](http://docs.aws.amazon.com/streams/latest/dev/developing-producers-with-kpl.html)
* [Generate Avro objects from schema sources](https://avro.apache.org/docs/1.8.1/gettingstartedjava.html)


#### Read stream from kinesis

In this part we read the messages from the kinesis stream that were sent by the producer.
We have created 2 kind of producers:
* KinesisConsumerApp: Main class that uses the KCL
* Describe: Main class that uses the Amazon SDK library

##### Run it
Execute main class in `KinesisConsumerApp` to run the KCL Worker. Hit Ctrl+Z to finish the reader.
Execute main class in `Describe` to run the Amazon SDK reader. Hit Ctrl+Z to finish the reader.

##### How it works
Both examples will be reading from the `TestStream` from the TRIM_HORIZON position (i.e. it will start from the oldest available data).
This class will use credentials stored in your .aws folder
If you do not have this folder on your computer just install the aws cli and type the following command:

``` bash
$ aws configure
AWS Access Key ID [None]: TYPE_YOUR_ACCESS_KEY
AWS Secret Access Key [None]: TYPE_YOUR_SECRET_KEY
Default region name [None]: us-west-1
Default output format [None]: ENTER
```
In order to run the KCL program, you will need an access/secret key with the following permissions:
- read access to Kinesis
- read/write access to dynamodb
- read/write access to metrics

This example basically builds a new kinesis Worker and starts it
``` java
KinesisClientLibConfiguration config = 
new KinesisClientLibConfiguration(APP_NAME, STREAM_NAME, credentials, workerId);

config.withIdleTimeBetweenReadsInMillis( 200 );
config.withInitialPositionInStream( DEFAULT_INITIAL_POSITION_IN_STREAM );
config.withRegionName(Region.getRegion(Regions.EU_WEST_1).getName());

IRecordProcessorFactory recordProcessorFactory = new SampleRecordProcessorFactory();
Worker worker = 
new Worker.Builder().recordProcessorFactory( recordProcessorFactory ).config( config ).build();

worker.run();
```

we need then to implement an `IRecordPRocessor` and add the processing logic in the `processSingleRecord` method.
In order to mark the consumed records as processed, we need to call the checkpoint method. This is done by the `RecordProcessor` every CHECKPOINT_INTERVAL_MILLIS (currently set to 1 minute).

``` java
if (System.currentTimeMillis() > nextCheckpointTimeInMillis) {
    checkpoint(processRecordsInput.getCheckpointer());
    nextCheckpointTimeInMillis = System.currentTimeMillis() + CHECKPOINT_INTERVAL_MILLIS;
}
```
This will add the last processed checkpoint in the dynamodb table.


The stream can also be consumed by using the Amazon SDK java library. 
As a demo, the class Describe.java does the following steps:

- connects to the Amazon kinesis endpoint 
- list the available streams
- reads data records from the first shard in the TestStream
- closes the kinesis client

This reader will only need read access to the kinesis Stream.

Apache Avro is used to serialize/deserialize the data payload inside the Record instance. The logic was created in the object AvroSerializer.

---


### TO DO

 - Write Unit Tests
 - It was not considered make an example of sending a message with the SDK because it seems that the KPL covers cases, but would be nice add an example of how to send it with the SDK.
 - Test Orika mapping with Domain Object (from GORM)