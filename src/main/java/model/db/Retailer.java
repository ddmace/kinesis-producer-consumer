package model.db;

import java.io.Serializable;

public class Retailer implements Serializable {
 public String getNormalizedName() {
  return normalizedName;
 }

 public void setNormalizedName(String normalizedName) {
  this.normalizedName = normalizedName;
 }

 public String getName() {
  return name;
 }

 public void setName(String name) {
  this.name = name;
 }

 public String getEmail() {
  return email;
 }

 public void setEmail(String email) {
  this.email = email;
 }

 public String getShortDescription() {
  return shortDescription;
 }

 public void setShortDescription(String shortDescription) {
  this.shortDescription = shortDescription;
 }

 public String getLargeDescription() {
  return largeDescription;
 }

 public void setLargeDescription(String largeDescription) {
  this.largeDescription = largeDescription;
 }

 private String normalizedName;
 private String name;
 private String email;
 private String shortDescription;
 private String largeDescription;
}
