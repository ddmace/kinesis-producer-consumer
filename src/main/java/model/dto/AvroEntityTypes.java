package model.dto;

/**
 * Created by marina on 9/08/16.
 */
public enum AvroEntityTypes {
    BROCHURE(1L),EVENT(2L);

    private long id;

    private AvroEntityTypes(long id) {
        this.id = id;
    }

    public long getId(){
        return id;
    }
}
