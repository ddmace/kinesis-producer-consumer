package model.dto;

import model.avro.AvroMsg;

import java.nio.ByteBuffer;
import java.util.Date;

/**
 * Created by marina on 4/08/16.
 */
public class ProducerDto {
    private String partitionKey;
    private AvroMsg avroMesg;
    private Object payloadDeserialized;

    public ProducerDto(String partitionKey, String action, AvroEntityTypes type, Object payloadDeserialized) {
        this.partitionKey = partitionKey;
        this.payloadDeserialized = payloadDeserialized;
        this.avroMesg = new AvroMsg();
        this.avroMesg.setAction(action);
        this.avroMesg.setSchemaId(type.getId());
        this.avroMesg.setTimestamp(new Date().getTime());
    }

    public String getPartitionKey() {
        return partitionKey;
    }

    public Object getPayloadDeserialized() {
        return payloadDeserialized;
    }

    public AvroMsg buildAvroMsg( String jsonPayload ) {
        avroMesg.setPayload(jsonPayload);
        return avroMesg;
    }

}
