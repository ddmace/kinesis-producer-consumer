package kinesis.consumer;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessorFactory;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.InitialPositionInStream;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.KinesisClientLibConfiguration;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.Worker;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.UUID;

/**
 * Main class that will create a Kinesis Consumer
 *
 */
public class KinesisConsumerApp {

    private static final Log log = LogFactory.getLog( KinesisConsumerApp.class );
    public static final String DEFAULT_STREAM_NAME = "TestStream";
    private static final String DEFAULT_APP_NAME = "kinesisSpike";
    private static final InitialPositionInStream DEFAULT_INITIAL_POSITION_IN_STREAM = InitialPositionInStream.TRIM_HORIZON;
    private static AWSCredentialsProvider credentialsProvider;

    /**
     * loads amazon credentials
     */
    private static void init() {
        // Ensure the JVM will refresh the cached IP values of AWS resources (e.g. service endpoints).
        java.security.Security.setProperty("networkaddress.cache.ttl", "60");

        /*
         * The ProfileCredentialsProvider will return your [default]
         * credential profile by reading from the credentials file located at
         * (~/.aws/credentials).
         */
        try {
            credentialsProvider = new ProfileCredentialsProvider();
            final AWSCredentials creds = credentialsProvider.getCredentials();

            log.info( "loaded with access key: " + creds.getAWSAccessKeyId());

        } catch (Exception e) {
            throw new AmazonClientException("Cannot load the credentials from the credential profiles file. " + "Please make sure that your credentials file is at the correct " + "location (~/.aws/credentials), and is in valid format.", e);
        }

    }

    public static void main(String[] args) throws UnknownHostException {

        log.info( "starting KinesisConsumerApp...");
        init();
        String workerId = InetAddress.getLocalHost().getCanonicalHostName() + ":" + UUID.randomUUID();

        log.info( "creating worker " + workerId);
        KinesisClientLibConfiguration config = new KinesisClientLibConfiguration(DEFAULT_APP_NAME, DEFAULT_STREAM_NAME, credentialsProvider, workerId);

        log.info( "** setting initial position to " + DEFAULT_INITIAL_POSITION_IN_STREAM );

        config.withIdleTimeBetweenReadsInMillis( 200 );
        config.withInitialPositionInStream( DEFAULT_INITIAL_POSITION_IN_STREAM );
        config.withRegionName(Region.getRegion(Regions.EU_WEST_1).getName());

        IRecordProcessorFactory recordProcessorFactory = new SampleRecordProcessorFactory();
        final Worker worker = new Worker.Builder().recordProcessorFactory( recordProcessorFactory ).config( config ).build();

        System.out.printf("Running %s to process stream %s as worker %s...\n", DEFAULT_APP_NAME, DEFAULT_STREAM_NAME, workerId);

        int exitCode = 0;
        try {
            worker.run();
        } catch (Throwable t) {
            System.err.println("Caught throwable while processing data.");
            t.printStackTrace();
            exitCode = 1;
        }

        System.exit(exitCode);

    }


}
