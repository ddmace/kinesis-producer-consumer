package model.db;

import java.io.Serializable;

public class Store implements Serializable {
 public String getName() {
  return name;
 }

 public void setName(String name) {
  this.name = name;
 }

 public String getEmail() {
  return email;
 }

 public void setEmail(String email) {
  this.email = email;
 }

 public String getWeb() {
  return web;
 }

 public void setWeb(String web) {
  this.web = web;
 }

 public String getTelephone() {
  return telephone;
 }

 public void setTelephone(String telephone) {
  this.telephone = telephone;
 }

 public String getFax() {
  return fax;
 }

 public void setFax(String fax) {
  this.fax = fax;
 }

 public String getDescription() {
  return description;
 }

 public void setDescription(String description) {
  this.description = description;
 }

 private String name;
 private String email;
 private String web;
 private String telephone;
 private String fax;
 private String description;
}
