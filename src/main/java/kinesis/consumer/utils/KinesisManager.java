package kinesis.consumer.utils;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.kinesis.AmazonKinesisClient;
import com.amazonaws.services.kinesis.model.*;

import java.util.concurrent.TimeUnit;

public class KinesisManager {

    public KinesisManager(AmazonKinesisClient client) {
        this.client = client;
    }

    /**
     * Creates a new Stream with the given parameters
     *
     * @param name Name of the String to be created
     * @param shards Number of shards to be created for the stream ( integer from 1 to 50 )
     * @return
     */
    public void createStream( String name, int shards ) {

        // Create a stream. The number of shards determines the provisioned throughput.
        CreateStreamRequest createStreamRequest = new CreateStreamRequest();
        createStreamRequest.setStreamName(name);
        createStreamRequest.setShardCount(shards);
        client.createStream(createStreamRequest);
        waitForStreamToBecomeAvailable(name);
    }

    /**
     * Deletes the given Stream
     *
     * @param name Name of the stream to be deleted
     * @return DeleteStreamResult
     */
    public DeleteStreamResult deleteStream(String name) {
        System.out.printf("Deleting the Amazon Kinesis stream used by the sample. Stream Name = %s.\n", name);
        return client.deleteStream( name );
    }

    private void waitForStreamToBecomeAvailable(String streamName) {
        System.out.printf("Waiting for %s to become ACTIVE...\n", streamName);

        long startTime = System.currentTimeMillis();
        long endTime = startTime + TimeUnit.MINUTES.toMillis(10);

        while (System.currentTimeMillis() < endTime) {

            try { Thread.sleep(TimeUnit.SECONDS.toMillis(20)); }catch( InterruptedException ignore ) { }

            try {
                DescribeStreamRequest describeStreamRequest = new DescribeStreamRequest();
                describeStreamRequest.setStreamName(streamName);
                // ask for no more than 10 shards at a time -- this is an optional parameter
                describeStreamRequest.setLimit(10);
                DescribeStreamResult describeStreamResponse = client.describeStream(describeStreamRequest);

                String streamStatus = describeStreamResponse.getStreamDescription().getStreamStatus();
                System.out.printf("\t- current state: %s\n", streamStatus);
                if ("ACTIVE".equals(streamStatus)) {
                    return;

                }

            } catch (ResourceNotFoundException ex) {
                // ResourceNotFound means the stream doesn't exist yet,
                // so ignore this error and just keep polling.
            } catch (AmazonServiceException ase) {
                throw ase;
            }

        }


        throw new RuntimeException(String.format("Stream %s never became active", streamName));
    }

    private AmazonKinesisClient client;
}
