package kinesis.consumer;

import avro.AvroSerializer;
import com.amazonaws.services.kinesis.clientlibrary.exceptions.InvalidStateException;
import com.amazonaws.services.kinesis.clientlibrary.exceptions.ShutdownException;
import com.amazonaws.services.kinesis.clientlibrary.exceptions.ThrottlingException;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorCheckpointer;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.types.InitializationInput;
import com.amazonaws.services.kinesis.clientlibrary.types.ProcessRecordsInput;
import com.amazonaws.services.kinesis.clientlibrary.types.ShutdownInput;
import com.amazonaws.services.kinesis.clientlibrary.types.ShutdownReason;
import com.amazonaws.services.kinesis.model.Record;
import model.avro.AvroBrochure;
import model.avro.AvroEvent;
import model.avro.AvroMsg;
import model.dto.AvroEntityTypes;
import org.apache.avro.Schema;
import org.apache.avro.reflect.ReflectData;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.List;

class RecordProcessor implements IRecordProcessor {

    public void initialize(final InitializationInput initializationInput) {
        log.info( "*** Initlializing record processor for shard: " + initializationInput.getShardId() );
        shardId = initializationInput.getShardId();
        serializer = AvroSerializer.instance();
    }

    public void processRecords(final ProcessRecordsInput processRecordsInput) {
        log.info( "*** processing " + String.valueOf(processRecordsInput.getRecords().size()) + " records from " + shardId );

        processRecordsWithRetries(processRecordsInput.getRecords());

        // Checkpoint once every checkpoint interval.
        if (System.currentTimeMillis() > nextCheckpointTimeInMillis) {
            checkpoint(processRecordsInput.getCheckpointer());
            nextCheckpointTimeInMillis = System.currentTimeMillis() + CHECKPOINT_INTERVAL_MILLIS;
        }

    }

    /**
     * Process records performing retries as needed. Skip "poison pill" records.
     *
     * @param records Data records to be processed.
     */
    private void processRecordsWithRetries(List<Record> records) {
        for (Record record : records) {
            boolean processedSuccessfully = false;
            for (int i = 0; i < NUM_RETRIES; i++) {
                try {
                    processSingleRecord(record);
                    processedSuccessfully = true;
                    break;
                } catch (Throwable t) {
                    log.warn("Caught throwable while processing record " + record, t);
                }


                // backoff if we encounter an exception.
                try {
                    Thread.sleep(BACKOFF_TIME_MILLIS);
                } catch (InterruptedException e) {
                    log.debug("Interrupted sleep", e);
                }

            }

            if (!processedSuccessfully) {
                log.error("Couldn't process record " + record + ". Skipping the record.");
            }

        }

    }

    /**
     * processes the given list of Records
     *
     * @param records
     */
    private void process(List<Record> records) {

        for( Record record: records ) {
            try {
                processSingleRecord( record );
            } catch (Throwable t) {
                log.warn("******** Caught throwable while processing record " + String.valueOf(record), t);
            }
        }

    }

    /**
     * Process a single record.
     *
     * @param record The record to be processed.
     */
    private void processSingleRecord(Record record) throws IOException {
        String data = null;
        try {

            Schema schema = ReflectData.get().getSchema( AvroMsg.class );
            final AvroMsg msg = serializer.deserialize( record.getData().array(), AvroMsg.class );
            log.info( "received AvroMsg: with schema-id: " + msg.getSchemaId() + ", action: " + msg.getAction());

            if( msg.getSchemaId().equals( AvroEntityTypes.BROCHURE.getId()) ) {

                schema = ReflectData.get().getSchema(AvroBrochure.class);
                final AvroBrochure event = serializer.deserializeJSON(msg.getPayload().toString(), AvroBrochure.class, schema);

                log.info( "received AvroBrochure: \'" + event.getName() + "\'" + event.getShortDescription());

            } else if( msg.getSchemaId().equals( AvroEntityTypes.EVENT.getId() )) {
                schema = ReflectData.get().getSchema(AvroEvent.class);
                final AvroEvent event = serializer.deserializeJSON(msg.getPayload().toString(), AvroEvent.class, schema);

                log.info( "received AvroEvent: \'" + event.getMessage() + "\'");
            }


            log.info(record.getSequenceNumber() + ", " + record.getPartitionKey() + ", " + data);

        } catch (CharacterCodingException e) {
            log.error("Malformed data: " + data, e);
        }

    }

    public void shutdown(ShutdownInput shutdownInput) {
        log.info("Shutting down record processor for shard: " + shardId);

        if (ShutdownReason.TERMINATE.equals(shutdownInput.getShutdownReason())) {
            checkpoint(shutdownInput.getCheckpointer());
        }


    }

    private void checkpoint(IRecordProcessorCheckpointer checkpointer) {
        log.info("Checkpointing shard " + shardId);
        for (int i = 0; i < NUM_RETRIES; i++) {
            try {
                checkpointer.checkpoint();
                break;
            } catch (ShutdownException se) {
                // Ignore checkpoint if the processor instance has been shutdown (fail over).
                log.info("Caught shutdown exception, skipping checkpoint.", se);
                break;
            } catch (ThrottlingException e) {
                // Backoff and re-attempt checkpoint upon transient failures
                if (i >= (NUM_RETRIES - 1)) {
                    log.error("Checkpoint failed after " + (i + 1) + "attempts.", e);
                    break;
                } else {
                    log.info("Transient issue when checkpointing - attempt " + (i + 1) + " of " + NUM_RETRIES, e);
                }

            } catch (InvalidStateException e) {
                // This indicates an issue with the DynamoDB table (check for table, provisioned IOPS).
                log.error("Cannot save checkpoint to the DynamoDB table used by the Amazon Kinesis Client Library.", e);
                break;
            }

            try {
                Thread.sleep(BACKOFF_TIME_MILLIS);
            } catch (InterruptedException e) {
                log.debug("Interrupted sleep", e);
            }

        }

    }

    private static final Log log = LogFactory.getLog(RecordProcessor.class);
    private static final long BACKOFF_TIME_MILLIS = 3000L;
    private static final int NUM_RETRIES = 10;
    private static final long CHECKPOINT_INTERVAL_MILLIS = 60000L;

    private long nextCheckpointTimeInMillis;
    private final CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();
    private String shardId;
    private AvroSerializer serializer;
}
