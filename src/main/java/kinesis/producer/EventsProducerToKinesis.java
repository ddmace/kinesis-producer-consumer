package kinesis.producer;

import avro.AvroSerializer;
import com.amazonaws.services.kinesis.producer.*;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import model.avro.AvroBrochure;
import model.avro.AvroEvent;
import model.avro.AvroMsg;
import model.avro.AvroStore;
import model.dto.ProducerDto;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.concurrent.BlockingQueue;

/**
 * Adapted from https://blogs.aws.amazon.com/bigdata/post/Tx3ET30EGDKUUI2/Implementing-Efficient-and-Reliable-Producers-with-the-Amazon-Kinesis-Producer-L
 *
 * other interesting doc
 * http://spotidoc.com/doc/1119460/amazon-kinesis-developer-guide
 * https://github.com/awslabs/amazon-kinesis-producer/tree/master/java/amazon-kinesis-producer-sample
 *
 * Created by marina on 1/08/16.
 */
public class EventsProducerToKinesis extends AbstractEventsProducer {

    private static final String STREAM_NAME   = "TestStream";
    private static final String REGION        = "eu-west-1";
    private static final Log log              = LogFactory.getLog(EventsProducerToKinesis.class.getName());

    private final KinesisProducer kinesis;
    private final AvroSerializer serializer = AvroSerializer.instance();

    public EventsProducerToKinesis(BlockingQueue<ProducerDto> inputQueue) {
        super(inputQueue);
        kinesis = new KinesisProducer(new KinesisProducerConfiguration()
                .setRegion(REGION));
    }

    private ByteBuffer serialize(Object object) throws IOException {
        if(object instanceof AvroBrochure) {
            return serializer.serialize(AvroBrochure.class, object);
        } else if(object instanceof AvroEvent) {
            return serializer.serialize(AvroEvent.class, object);
        } else if(object instanceof AvroStore) {
            return serializer.serialize(AvroStore.class, object);
        } else if(object instanceof AvroMsg) {
            return serializer.serialize(AvroMsg.class, object);
        }
        throw new IllegalArgumentException("Not parseable object");
    }

    private String serializeJSON(Object object) throws IOException {
        if(object instanceof AvroBrochure) {
            return serializer.serializeJSON( AvroBrochure.class, object, ((AvroBrochure) object).getSchema() );
        } else if(object instanceof AvroEvent) {
            return serializer.serializeJSON( AvroEvent.class, object, ((AvroEvent) object).getSchema() );
        } else if(object instanceof AvroStore) {
            return serializer.serializeJSON( AvroStore.class, object, ((AvroStore) object).getSchema() );
        } else if(object instanceof AvroMsg) {
            return serializer.serializeJSON( AvroMsg.class, object, ((AvroMsg) object).getSchema() );
        }
        throw new IllegalArgumentException("Not parseable object");
    }

    @Override
    protected void runOnce() throws Exception {

        ProducerDto event = inputQueue.take();
        final String partitionKey = event.getPartitionKey();
        final Object payload = event.getPayloadDeserialized();

        //serialize all message
        String obj = serializeJSON( payload );
        final AvroMsg message =  event.buildAvroMsg(obj);
        ByteBuffer data = serialize(message);

        while (kinesis.getOutstandingRecordsCount() > 1e4) {
            Thread.sleep(1);
        }
        recordsPut.getAndIncrement();

        ListenableFuture<UserRecordResult> f = kinesis.addUserRecord(STREAM_NAME, partitionKey, data);

        Futures.addCallback(f, new FutureCallback<UserRecordResult>() {
            @Override
            public void onSuccess(UserRecordResult result) {
                long totalTime = 0;
                for(Attempt a : result.getAttempts()) {
                    totalTime += a.getDelay() + a.getDuration();
                }

                log.info(String.format(
                        "Succesfully put record, partitionKey=%s, payload=%s, sequenceNumber=%s, shardId=%s, took %d attempts, totalling %s ms",
                        partitionKey, message, result.getSequenceNumber(),
                        result.getShardId(), result.getAttempts().size(),
                        totalTime));
            }

            @Override
            public void onFailure(Throwable t) {
                if (t instanceof UserRecordFailedException) {
                    UserRecordFailedException e = (UserRecordFailedException) t;
                    UserRecordResult result = e.getResult();

                    StringBuilder errorList = new StringBuilder();
                    for(Attempt a : result.getAttempts()) {
                        errorList.append(String.format(
                                "Delay after prev attempt: %d ms, Duration: %d ms, Code: %s, Message: %s",
                                a.getDelay(), a.getDuration(),
                                a.getErrorCode(),
                                a.getErrorMessage())).append("\n");
                    }

                    log.error(String.format( "Record failed to put, partitionKey=%s, payload=%s, attempts:\n%s", partitionKey, message, errorList.toString()));
                }
            };
        });

    }

    @Override
    public long recordsPut() {
        try {
            long sum = 0;
            for(Metric m : kinesis.getMetrics("UserRecordsPut")) {
                if(m.getDimensions().size() == 2) {
                    sum = (long) m.getSum();
                }
            }
            return  sum;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void stop() {
        super.stop();
        kinesis.flushSync();
        kinesis.destroy();
    }
}

